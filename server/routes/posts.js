const express = require('express');
const router = express.Router();
const Post = require('../models/Posts');

//get all post
router.get('/', async (req, res) => {
    try{
         const posts = await Post.find();
         res.json(posts);
    }catch(err){
        res.json({message: err});
    }
});

//Add a new post
router.post('/', async (req,res) => {
    console.log(req.file);
    const post = new Post({
        title: req.body.title,
        image: req.body.image
    });
    try{   
    const savedPost = await post.save()
        res.json(savedPost)
    }catch (err) {
       res.json({message: err}); 
    }
});


//Select a post
router.get('/:postId', async (req,res) => {
    try {
         const post = await Post.findById(req.params.postId);
         res.json(post);
    }catch (err) {
        res.json({message: err});
    }    
});


//Delete Post
router.delete('/:postId', async (req,res) => {
    try {
    const removedPost = await Post.remove({_id: req.params.postId}) 
    res.json(removedPost);
    }catch (err) {
    res.json({message: err});
    }
});


//Update a post
router.patch('/:postId', async (req,res) => {
    try{
    const updatedPost = await Post.updateOne(
        {_id: req.params.postId}, 
        {$set: {title: req.body.title, image: req.body.image}}
     );
     res.json(updatedPost);    
    }catch (err) {
    res.json({message: err});
    }
})


module.exports = router;