const express = require('express');
const router = express.Router();
const Artist = require('../models/Artists');

//Get All artists
router.get('/', async (req, res) => {
    try{
         const artists = await Artist.find();
         res.json(artists);
    }catch(err){
        res.json({message: err});
    }
});

//Add a new artist data
router.post('/', async (req,res) => {
    console.log(req.file);
    const artist = new Artist({
        name: req.body.name,
        image: req.body.image,
        dob: req.body.dob,
        hobby: req.body.hobby,
        album: req.body.album,
        anime: req.body.anime
    });
    try{   
    const savedArtist = await artist.save()
        res.json(savedArtist)
    }catch (err) {
       res.json({message: err}); 
    }
});

//Select a Artist
router.get('/:artistId', async (req,res) => {
    try {
         const artist = await Artist.findById(req.params.artistId);
         res.json(artist);
    }catch (err) {
        res.json({message: err});
    }    
});

//Delete a Artist Data
router.delete('/:artistId', async (req,res) => {
    try {
    const removedArtist = await Artist.remove({_id: req.params.artistId}) 
    res.json(removedArtist);
    }catch (err) {
    res.json({message: err});
    }
});


//Update a Artist data
router.patch('/:artistId', async (req,res) => {
    try{
    const updatedArtist = await Artist.updateOne(
        {_id: req.params.artistId}, 
        {$set: {name: req.body.name, 
                image: req.body.image,
                dob: req.body.dob,
                hobby: req.body.hobby,
                album: req.body.album,
                anime: req.body.anime}}
     );
     res.json(updatedArtist);    
    }catch (err) {
    res.json({message: err});
    }
})

module.exports = router;