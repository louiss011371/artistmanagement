const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');
const cors = require('cors')

app.use(bodyParser.json());
app.use(express.json({ extended: false}));
app.use(cors());

//Import Routes
const postsRoute = require('./routes/posts');
app.use('/posts', postsRoute);

const booksRoute = require('./routes/books')
app.use('/books', booksRoute)

const artistsRoute = require('./routes/artists')
app.use('/artists', artistsRoute)

//Routes
app.get('/', (req,res) => {
    res.send('testing server');
});

//Connect to DB
mongoose.connect(
process.env.DB_CONNECTION,
{ useNewUrlParser: true }, 
() => console.log('connected to DB!')
);

//How to we start Listening the server
app.listen(3000);
